package com.example.tsvqt.chatapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tsvqt.chatapp.Adapters.SectionsPagerAdapter;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.AppCollections;
import com.example.tsvqt.chatapp.OnlineIntentService;
import com.example.tsvqt.chatapp.R;
import com.example.tsvqt.chatapp.SharedPreferencesManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;
    private FirebaseUser currentUser;

    private Toolbar mToolbar;

    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        SharedPreferencesManager.init(getApplicationContext());
        // Check if user is signed in (non-null) and update UI accordingly.
        if(currentUser == null){
            sendToStart();
        }else {
            mUserRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users).child(currentUser.getUid());
            mToolbar = findViewById(R.id.main_page_toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(R.string.Chat);

            mViewPager = findViewById(R.id.main_tabPager);
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            mViewPager.setAdapter(mSectionsPagerAdapter);

            mTabLayout = findViewById(R.id.main_tabs);
            mTabLayout.setupWithViewPager(mViewPager);
            mTabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));

            //Online Service
            Intent intent = new Intent(this, OnlineIntentService.class);
            mUserRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users).child(currentUser.getUid());
            mUserRef.child(AppChildren.online).setValue(true);
            Log.e("ONLINE_SERVICE: ", "ALIVE");
            startService(intent);
        }
    }

/*
    @Override
    protected void onStop() {
        super.onStop();
        mUserRef.child(AppChildren.online).setValue(false);
    }
*/
    private void sendToStart() {
        Intent startIntent = new Intent(MainActivity.this, StartActivity.class);
        startActivity(startIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.main_logout_button){
            mUserRef.child(AppChildren.online).setValue(false);
            mUserRef.child(AppChildren.lastSeen).setValue(ServerValue.TIMESTAMP);
            FirebaseAuth.getInstance().signOut();
            SharedPreferencesManager.getsInstance().clearSharedPreferences();
            sendToStart();
        }

        if(item.getItemId() == R.id.main_settings_button){
            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsIntent);
        }

        if(item.getItemId() == R.id.main_all_btn){
            Intent allUsersIntent = new Intent(MainActivity.this, UsersActivity.class);
            startActivity(allUsersIntent);
        }

        if(item.getItemId() == R.id.main_notifications){
            Intent notifications = new Intent(MainActivity.this, FriendRequestsActivity.class);
            startActivity(notifications);
        }

        return true;
    }
}
