package com.example.tsvqt.chatapp.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tsvqt.chatapp.Fragments.ChatsFragment;
import com.example.tsvqt.chatapp.Fragments.FriendsFragment;
import com.example.tsvqt.chatapp.Fragments.RequestsFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter{

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new ChatsFragment();
            case 1:
                return new FriendsFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "CHATS";
            case 1:
                return "FRIENDS";

            default:
                return null;
        }
    }
}
