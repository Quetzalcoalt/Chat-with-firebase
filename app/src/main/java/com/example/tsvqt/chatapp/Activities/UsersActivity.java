package com.example.tsvqt.chatapp.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.AppCollections;
import com.example.tsvqt.chatapp.Models.UsersModel;
import com.example.tsvqt.chatapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UsersActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView mUsersRlist;

    private DatabaseReference mUsersDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users);

        mToolbar = findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.All_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUsersRlist = findViewById(R.id.users_rList);

        mUsersRlist.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<UsersModel> options = new FirebaseRecyclerOptions.Builder<UsersModel>()
                .setQuery(mUsersDatabase,UsersModel.class)
                .build();

        FirebaseRecyclerAdapter<UsersModel, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<UsersModel, UsersViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull UsersViewHolder holder, int position, @NonNull UsersModel model) {
                holder.userNameTV.setText(model.getName());
                holder.userStatusTV.setText(model.getStatus());
                Glide.with(UsersActivity.this).load(model.getThumb_image()).apply(RequestOptions.circleCropTransform()).into(holder.userIV);

                final String user_id = getRef(position).getKey();

                holder.userIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(UsersActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(AppChildren.user_id, user_id);
                        startActivity(profileIntent);
                    }
                });
            }

            @NonNull
            @Override
            public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_layout, parent,false);
                return new UsersViewHolder(view);
            }
        };
        firebaseRecyclerAdapter.startListening();
        mUsersRlist.setAdapter(firebaseRecyclerAdapter);
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder{

        private View mView;
        private ImageView userIV;
        private TextView userNameTV, userStatusTV;

        public UsersViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            userIV = itemView.findViewById(R.id.users_item_image);
            userNameTV = itemView.findViewById(R.id.users_item_name);
            userStatusTV = itemView.findViewById(R.id.users_item_status);
        }
    }
}
