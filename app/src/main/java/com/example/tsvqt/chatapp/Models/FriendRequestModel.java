package com.example.tsvqt.chatapp.Models;

public class FriendRequestModel {

    private String name, thumb_image;

    public FriendRequestModel(){

    }

    public FriendRequestModel(String name, String thumb_image) {
        this.name = name;
        this.thumb_image = thumb_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }
}
