package com.example.tsvqt.chatapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

public class OnlineIntentService extends Service {

    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        mAuth = FirebaseAuth.getInstance();
        mUserRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users).child(mAuth.getCurrentUser().getUid());
        mUserRef.child(AppChildren.online).setValue(false);
        mUserRef.child(AppChildren.lastSeen).setValue(ServerValue.TIMESTAMP);
        Log.e("ONLINE_SERVICE: ", "REMOVED");
        super.onTaskRemoved(rootIntent);
    }
}
