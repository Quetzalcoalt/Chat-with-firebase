package com.example.tsvqt.chatapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

public class SharedPreferencesManager {

    private static final String TAG = SharedPreferencesManager.class.getSimpleName();

    private static Context context;
    private static SharedPreferencesManager sInstance;
    private final SharedPreferences mPref;
    private final SharedPreferences.Editor editor;


    private static final String SHARED_PREF_NAME = "chatNotificationSharedPref";
    private static final String KEY_ACCESS_TOKEN = "token";
    private static final String USER_ID = "userID";
    private static final String USER_NAME = "username";
    private static final String USER_THUMB_IMAGE = "userThumbImage";

    private SharedPreferencesManager(Context context) {
        mPref = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = mPref.edit();
    }

    public static synchronized void init (Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferencesManager(context);
        }
    }

    public static synchronized  SharedPreferencesManager getsInstance(){
        if(sInstance == null){
            throw new IllegalStateException(SharedPreferencesManager.class.getSimpleName() +
                    " is not initialized, call init(..) method first.");
        }
        return sInstance;
    }

    public void storeUser(String userID, String userName, String userThumbNail){
        editor.putString(USER_ID, userID);
        editor.putString(USER_NAME, userName);
        editor.putString(USER_THUMB_IMAGE, userThumbNail);
        editor.apply();
    }

    public HashMap<String, String> getUser(){
        HashMap<String, String> user = new HashMap<>();
        user.put(USER_ID, mPref.getString(USER_ID, null));
        user.put(USER_NAME, mPref.getString(USER_NAME, null));
        user.put(USER_THUMB_IMAGE, mPref.getString(USER_THUMB_IMAGE, null));
        return user;
    }

    public void storeUserThumbImage(String thumbImage){
        editor.putString(USER_THUMB_IMAGE, thumbImage);
        editor.apply();
    }

    public String getUserID(){
        return mPref.getString(USER_ID, null);
    }

    public String getUserName(){
        return mPref.getString(USER_NAME, null);
    }

    public String getUserThumbImage(){
        return mPref.getString(USER_THUMB_IMAGE, null);
    }

    public boolean storeToken(String token){
        editor.putString(KEY_ACCESS_TOKEN, token);
        editor.apply();
        return true;
    }

    public String getToken(){
        return mPref.getString(KEY_ACCESS_TOKEN, null);
    }

    public void clearSharedPreferences() {
        editor.clear();
        editor.apply();

        Log.d(TAG, "Deleted all user info from SharePreferences");
    }
}
