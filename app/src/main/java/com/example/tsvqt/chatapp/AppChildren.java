package com.example.tsvqt.chatapp;

public class AppChildren {

    public static String device_token = "device_token";
    public static String user_id = "user_id";
    public static String name = "name";
    public static String thumb_image = "thumb_image";
    public static String status = "status";
    public static String image = "image";
    public static String defaultImage = "default";
    public static String status_value = "status_value";
    public static String request_type = "request_type";
    public static String online = "online";
    public static String lastSeen = "last_seen";
    public static String seen = "seen";
    public static String timestamp = "timestamp";
    public static String message = "message";
    public static String type = "type";
    public static String text = "text";
    public static String from = "from";
    public static String currentUserImage = "currentUserImage";

}
