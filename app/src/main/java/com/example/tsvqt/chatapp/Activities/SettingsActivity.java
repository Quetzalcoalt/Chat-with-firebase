package com.example.tsvqt.chatapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {

    private final static String TAG = "SettingsActivity";

    private DatabaseReference mUserDatabase;
    private FirebaseUser mCurrentUser;

    private ImageView mImageView;
    private TextView mName, mStatus;
    private Button mChangeImageBtn, mChangeStatusBtn;

    private static final int GALLERY_PICK = 1;

    private StorageReference mImageStorage;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mImageStorage = FirebaseStorage.getInstance().getReference();

        mImageView = findViewById(R.id.settings_image);
        mName = findViewById(R.id.settings_display_name);
        mStatus = findViewById(R.id.settings_status);

        mChangeImageBtn = findViewById(R.id.settings_iamge_btn);
        mChangeStatusBtn = findViewById(R.id.settings_status_btn);

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        String current_uid = mCurrentUser.getUid();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
        mUserDatabase.keepSynced(true);

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child(AppChildren.name).getValue().toString();
                String image = dataSnapshot.child(AppChildren.image).getValue().toString();
                String status = dataSnapshot.child(AppChildren.status).getValue().toString();
                String thumb_image = dataSnapshot.child(AppChildren.thumb_image).getValue().toString();

                mName.setText(name);
                mStatus.setText(status);
                if(!image.equals(AppChildren.defaultImage)){
                    Glide.with(getApplicationContext()).load(thumb_image).apply(RequestOptions.circleCropTransform()).into(mImageView);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mChangeStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent status_intent = new Intent(SettingsActivity.this, StatusActivity.class);
                status_intent.putExtra(AppChildren.status_value, mStatus.getText().toString());
                startActivity(status_intent);
            }
        });

        mChangeImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(galleryIntent, getString(R.string.Select_image)), GALLERY_PICK);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {

            Uri imageURI = data.getData();
            CropImage.activity(imageURI)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setTitle(getString(R.string.Uploading_image));
                mProgressDialog.setMessage(getString(R.string.Please_wait_dialog));
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();

                Uri originalImage = result.getOriginalUri();
                Uri resultUri = result.getUri();
                String current_user_id = mCurrentUser.getUid();
                File thumb_filePath = new File(resultUri.getPath());
                Bitmap thumb_bitmap;
                byte[] thumb_byte_array = null;
                try {
                    thumb_bitmap = new Compressor(this)
                            .setMaxHeight(640)
                            .setMaxWidth(640)
                            .setQuality(75)
                            .compressToBitmap(thumb_filePath);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    thumb_byte_array = baos.toByteArray();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                final StorageReference mainImageRef = mImageStorage.child("profile-images").child(current_user_id + ".jpg");
                final StorageReference thumbRef = mImageStorage.child("thumb_nails").child(current_user_id + ".jpg");

                /**
                 * Uploads main image first, if successful uploads the thumbnail.
                 */
                if(thumb_byte_array == null) {
                    mProgressDialog.dismiss();
                    Toast.makeText(SettingsActivity.this, "Error while uploading!", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "thumbnail byte array is empty!");
                }else{
                    UploadTask uploadTaskMainImage = mainImageRef.putFile(originalImage);
                    final UploadTask uploadTaskThumbnail = thumbRef.putBytes(thumb_byte_array);
                    Task<Uri> urlTaskMainImage = uploadTaskMainImage.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if(!task.isSuccessful()){
                                Log.e(TAG, task.getException().toString());
                                mProgressDialog.dismiss();
                                Toast.makeText(SettingsActivity.this, "Upload error", Toast.LENGTH_SHORT).show();
                            }
                            return mainImageRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if(task.isSuccessful()){
                                final Uri downloadUri = task.getResult();

                                Task<Uri> urlTaskThumbnail = uploadTaskThumbnail.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                    @Override
                                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                        if(!task.isSuccessful()){
                                            Log.e(TAG, task.getException().toString());
                                            mProgressDialog.dismiss();
                                            Toast.makeText(SettingsActivity.this, "Upload error (thumbnail)", Toast.LENGTH_SHORT).show();
                                        }
                                        return thumbRef.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if(task.isSuccessful()) {
                                            Uri downloadUriThumbnail = task.getResult();

                                            Map<String, Object> update_hashMap = new HashMap<>();

                                            update_hashMap.put("image", downloadUri.toString());
                                            update_hashMap.put("thumb_image", downloadUriThumbnail.toString());
                                            mUserDatabase.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()) {
                                                        mProgressDialog.dismiss();
                                                        Toast.makeText(SettingsActivity.this, "Upload successful", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });

                            }else{
                                Log.e(TAG, task.getException().toString());
                                mProgressDialog.dismiss();
                                Toast.makeText(SettingsActivity.this, "Upload task did not succeed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
