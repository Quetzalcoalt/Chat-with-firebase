package com.example.tsvqt.chatapp.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.AppCollections;
import com.example.tsvqt.chatapp.Models.FriendRequestModel;
import com.example.tsvqt.chatapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FriendRequestsActivity extends AppCompatActivity {

    private static String TAG = "FriendRequestsActivity";

    private Toolbar mToolbar;
    private RecyclerView mFriendsRequestsRV;

    private ArrayList<FriendRequestModel> friendRequestArrayList;

    private FirebaseAuth mAuth;
    private String mCurrentUserID;
    private DatabaseReference mFriendRequestRef, mUsersRef, mRootRef;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_requests);
        friendRequestArrayList = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserID = mAuth.getCurrentUser().getUid();

        mRootRef = FirebaseDatabase.getInstance().getReference();

        mFriendRequestRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Friend_req).child(mCurrentUserID);
        mFriendRequestRef.keepSynced(true);

        mUsersRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users);
        mUsersRef.keepSynced(true);

        //Log.e(TAG, mFriendRequestRef);

        mToolbar = findViewById(R.id.friend_request_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.Friend_requests);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFriendsRequestsRV = findViewById(R.id.friend_request_RV);
        mFriendsRequestsRV.setLayoutManager(new LinearLayoutManager(this));
        mFriendsRequestsRV.setHasFixedSize(true);
        DividerItemDecoration itemDecor = new DividerItemDecoration(FriendRequestsActivity.this, DividerItemDecoration.VERTICAL);
        mFriendsRequestsRV.addItemDecoration(itemDecor);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<FriendRequestModel> options = new FirebaseRecyclerOptions.Builder<FriendRequestModel>()
                .setQuery(mFriendRequestRef,FriendRequestModel.class)
                .build();

        FirebaseRecyclerAdapter<FriendRequestModel, FriendRequestViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<FriendRequestModel, FriendRequestViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final FriendRequestViewHolder holder, int position, @NonNull final FriendRequestModel model) {

                final String list_user_id=getRef(position).getKey();
               // Log.e(TAG, "List_user_id: " + list_user_id);
                mUsersRef.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final String userName = dataSnapshot.child(AppChildren.name).getValue().toString();
                        String userThumbImage = dataSnapshot.child(AppChildren.thumb_image).getValue().toString();
                        //Log.e(TAG, "userName: " + userName);
                        //Log.e(TAG, "thumbnail: " + userThumbImage);
                        holder.userNameTV.setText(userName);
                        Glide.with(FriendRequestsActivity.this).load(userThumbImage).apply(RequestOptions.circleCropTransform()).into(holder.userIV);

                        final String user_id = getRef(holder.getAdapterPosition()).getKey();

                        holder.userIV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent profileIntent = new Intent(FriendRequestsActivity.this, ProfileActivity.class);
                                profileIntent.putExtra(AppChildren.user_id, user_id);
                                startActivity(profileIntent);
                            }
                        });

                        holder.acceptBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final String currentDate = DateFormat.getDateInstance().format(new Date());

                                Map<String, Object> friendsMap = new HashMap<>();
                                friendsMap.put(AppCollections.Friends + "/" + mCurrentUserID + "/" + user_id + "/date", currentDate);
                                friendsMap.put(AppCollections.Friends + "/" + user_id + "/" + mCurrentUserID + "/date", currentDate);

                                friendsMap.put(AppCollections.Friend_req + "/" + mCurrentUserID + "/" + user_id, null);
                                friendsMap.put(AppCollections.Friend_req + "/" + user_id + "/" + mCurrentUserID, null);

                                mRootRef.updateChildren(friendsMap, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        if(databaseError == null){
                                            Toast.makeText(FriendRequestsActivity.this, "You are now friends with " + userName, Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(FriendRequestsActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        });

                        holder.declineBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @NonNull
            @Override
            public FriendRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_request_item, parent,false);
                return new FriendRequestViewHolder(view);
            }
        };
        firebaseRecyclerAdapter.startListening();
        mFriendsRequestsRV.setAdapter(firebaseRecyclerAdapter);
    }

    public static class FriendRequestViewHolder extends RecyclerView.ViewHolder{

        private ImageView userIV;
        private TextView userNameTV;
        private Button acceptBtn, declineBtn;

        public FriendRequestViewHolder(View itemView) {
            super(itemView);
            userIV = itemView.findViewById(R.id.friends_request_image);
            userNameTV = itemView.findViewById(R.id.friends_request_userName);
            acceptBtn = itemView.findViewById(R.id.friends_request_accept_btn);
            declineBtn = itemView.findViewById(R.id.friends_request_decline_btn);
        }
    }
}
