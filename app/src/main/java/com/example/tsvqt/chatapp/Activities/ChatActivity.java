package com.example.tsvqt.chatapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.Adapters.MessagesAdapter;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.AppCollections;
import com.example.tsvqt.chatapp.GetTimeAgo;
import com.example.tsvqt.chatapp.Models.MessagesModel;
import com.example.tsvqt.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "ChatActivity";

    private String mChatUser, mChatUserName, mChatUserImage;

    private ImageView mProfileImage, mChatAddBtn, mChatSendBtn;
    private TextView mToolBarTitle, mToolbarLastSeen;
    private Toolbar mChatToolbar;
    private ActionBar actionBar;
    private EditText mChatMessageET;
    private RecyclerView mMessagesRV;
    private List<MessagesModel> messagesList;
    private MessagesAdapter messagesAdapter;

    private DatabaseReference mRootRef;
    private DatabaseReference mMessageRef;
    private FirebaseAuth mAuth;
    private String mCurrentUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mCurrentUserID = mAuth.getCurrentUser().getUid();

        Intent getIntent = getIntent();
        mChatUser = getIntent.getStringExtra(AppChildren.user_id);
        mChatUserName = getIntent.getStringExtra(AppChildren.name);
        mChatUserImage = getIntent.getStringExtra(AppChildren.thumb_image);

        mChatToolbar = findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);

        actionBar.setCustomView(action_bar_view);

        // ---- Custom Action bar Items ----//

        mToolBarTitle = findViewById(R.id.custom_bar_username);
        mToolbarLastSeen = findViewById(R.id.custom_bar_last_seen);
        mProfileImage = findViewById(R.id.custom_bar_image);

        mMessagesRV = findViewById(R.id.chat_app_messages_recycler_view);
        mMessagesRV.setHasFixedSize(true);
        mMessagesRV.setLayoutManager(new LinearLayoutManager(this));
        messagesList = new ArrayList<>();
        messagesAdapter = new MessagesAdapter(this, messagesList);
        messagesAdapter.setUserName(mChatUserName);
        messagesAdapter.setUserImage(mChatUserImage);
        mMessagesRV.setAdapter(messagesAdapter);

        loadMessage();

        mChatAddBtn = findViewById(R.id.chat_add_btn);
        mChatMessageET = findViewById(R.id.chat_message_edit_text);
        mChatSendBtn = findViewById(R.id.chat_sent_btn);

        mToolBarTitle.setText(mChatUserName);
        Glide.with(ChatActivity.this).load(mChatUserImage).apply(RequestOptions.circleCropTransform()).into(mProfileImage);

        mRootRef.child(AppCollections.Users).child(mChatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean online = (boolean) dataSnapshot.child(AppChildren.online).getValue();
                if(online){
                    mToolbarLastSeen.setText(R.string.Online);
                }else{
                    String lastSeen = GetTimeAgo.getTimeAgo(Long.parseLong(dataSnapshot.child(AppChildren.lastSeen).getValue().toString()), getApplicationContext());
                    mToolbarLastSeen.setText(lastSeen);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mRootRef.child(AppCollections.Chat).child(mCurrentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChild(mChatUser)){
                    Map<String, Object> chatAddMap = new HashMap<>();
                    chatAddMap.put(AppChildren.seen, false);
                    chatAddMap.put(AppChildren.timestamp, ServerValue.TIMESTAMP);

                    Map<String, Object> chatUserMap = new HashMap<>();
                    chatUserMap.put(AppCollections.Chat + "/" + mCurrentUserID + "/" + mChatUser, chatAddMap);
                    chatUserMap.put(AppCollections.Chat + "/" + mChatUser + "/" + mCurrentUserID, chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.e(TAG, databaseError.getMessage());
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }


        });

    }

    private void loadMessage() {
        mMessageRef = mRootRef.child(AppCollections.Messages).child(mCurrentUserID).child(mChatUser);

        mMessageRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MessagesModel message = dataSnapshot.getValue(MessagesModel.class);

                messagesList.add(message);
                messagesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {
        String message = mChatMessageET.getText().toString();
        if(!TextUtils.isEmpty(message)){

            String current_user_ref = AppCollections.Messages + "/" + mCurrentUserID + "/" + mChatUser;
            String chat_user_ref = AppCollections.Messages + "/" + mChatUser + "/" + mCurrentUserID;

            DatabaseReference user_message_push = mRootRef.child(AppCollections.Messages).child(mCurrentUserID).child(mChatUser).push();

            String pushID = user_message_push.getKey();

            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put(AppChildren.message, message);
            messageMap.put(AppChildren.seen, false);
            messageMap.put(AppChildren.type, AppChildren.text);
            messageMap.put(AppChildren.timestamp, ServerValue.TIMESTAMP);
            messageMap.put(AppChildren.from, mCurrentUserID);

            Map<String, Object> messageUserMap = new HashMap<>();
            messageUserMap.put(current_user_ref + "/" + pushID, messageMap);
            messageUserMap.put(chat_user_ref + "/" + pushID, messageMap);

            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Log.e(TAG, databaseError.getMessage());
                    }
                    mChatMessageET.setText("");
                }
            });
        }
    }
}
