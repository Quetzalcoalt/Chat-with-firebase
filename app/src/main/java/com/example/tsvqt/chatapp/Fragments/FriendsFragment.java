package com.example.tsvqt.chatapp.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.Activities.ChatActivity;
import com.example.tsvqt.chatapp.Activities.ProfileActivity;
import com.example.tsvqt.chatapp.AppChildren;
import com.example.tsvqt.chatapp.AppCollections;
import com.example.tsvqt.chatapp.Models.FriendRequestModel;
import com.example.tsvqt.chatapp.Models.FriendsModel;
import com.example.tsvqt.chatapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    private static final String TAG = "FriendsFragment";

    private RecyclerView mFriendsList;

    private DatabaseReference mFriendsRef;
    private DatabaseReference mUsersRef;
    private FirebaseAuth mAuth;

    private String mCurrentUserID, mCurrentUserImage;

    private View mMainView;

    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_friends, container, false);

        mFriendsList = mMainView.findViewById(R.id.friends_fragment_recyclerView);
        mAuth = FirebaseAuth.getInstance();

        mCurrentUserID = mAuth.getCurrentUser().getUid();

        mFriendsRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Friends).child(mCurrentUserID);
        mFriendsRef.keepSynced(true);
        mUsersRef = FirebaseDatabase.getInstance().getReference().child(AppCollections.Users);
        mUsersRef.keepSynced(true);

        mCurrentUserImage = mUsersRef.child(mCurrentUserID).child(AppChildren.thumb_image).getKey();

        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerOptions<FriendsModel> options = new FirebaseRecyclerOptions.Builder<FriendsModel>()
                .setQuery(mFriendsRef,FriendsModel.class)
                .build();

        FirebaseRecyclerAdapter<FriendsModel, FriendsViewHolder> friendsRecyclerViewAdapter = new FirebaseRecyclerAdapter<FriendsModel, FriendsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final FriendsViewHolder holder, int position, @NonNull FriendsModel model) {
                holder.userOnline.setVisibility(View.VISIBLE);

                holder.userStatus.setText(model.getDate());
                final String list_user_id = getRef(position).getKey();

                mUsersRef.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final String userName = dataSnapshot.child(AppChildren.name).getValue().toString();
                        final String thumbnail = dataSnapshot.child(AppChildren.thumb_image).getValue().toString();

                        holder.userName.setText(userName);
                        if(isAdded()) {
                            Glide.with(getActivity()).load(thumbnail).apply(RequestOptions.circleCropTransform()).into(holder.userImage);
                        }

                        if(dataSnapshot.hasChild(AppChildren.online)) {
                            boolean online = (boolean) dataSnapshot.child(AppChildren.online).getValue();
                            if (online) {
                                holder.userOnline.setImageDrawable(getResources().getDrawable(android.R.drawable.presence_online));
                            } else {
                                holder.userOnline.setImageDrawable(getResources().getDrawable(android.R.drawable.presence_invisible));
                            }
                        }

                        holder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CharSequence options[] = new CharSequence[]{"Open profile", "Send message"};

                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                                builder.setTitle(R.string.Select_options);
                                builder.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch(which){
                                            case 0:
                                                Intent profileItent = new Intent(getContext(), ProfileActivity.class);
                                                profileItent.putExtra(AppChildren.user_id, list_user_id);
                                                startActivity(profileItent);
                                                break;
                                            case 1:
                                                Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                                chatIntent.putExtra(AppChildren.user_id, list_user_id);
                                                chatIntent.putExtra(AppChildren.name, userName);
                                                chatIntent.putExtra(AppChildren.thumb_image, thumbnail);
                                                chatIntent.putExtra(AppChildren.currentUserImage, mCurrentUserImage);
                                                startActivity(chatIntent);
                                                break;

                                                default: break;
                                        }
                                    }
                                });
                                builder.show();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_layout, parent,false);
                return new FriendsViewHolder(view);
            }
        };
        friendsRecyclerViewAdapter.startListening();
        mFriendsList.setAdapter(friendsRecyclerViewAdapter);
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder{

        private View mView;
        private TextView userStatus, userName;
        private ImageView userImage, userOnline;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            userStatus = itemView.findViewById(R.id.users_item_status);
            userName = itemView.findViewById(R.id.users_item_name);
            userImage = itemView.findViewById(R.id.users_item_image);
            userOnline = itemView.findViewById(R.id.users_item_online);
        }
    }
}
