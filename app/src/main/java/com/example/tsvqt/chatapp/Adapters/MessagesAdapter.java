package com.example.tsvqt.chatapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.chatapp.Models.MessagesModel;
import com.example.tsvqt.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private static final String TAG = MessagesAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<MessagesModel> mMessagesList;
    private Context context;

    private String userName, userImage, currentUserID, mCurrentUserImage;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public MessagesAdapter(Context context, List<MessagesModel> mMessagesList){
        inflater = LayoutInflater.from(context);
        this.mMessagesList = mMessagesList;
        this.context = context;
        currentUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.message_item_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder called " + position);
        MessagesModel msg = mMessagesList.get(position);
        String from_user = msg.getFrom();
        if(from_user.equals(currentUserID)){
            holder.messageText.setBackgroundColor(Color.WHITE);
            holder.messageText.setTextColor(Color.BLACK);
            Glide.with(context).load(userImage).apply(RequestOptions.circleCropTransform()).into(holder.profileImage);
        }else{
            holder.messageText.setBackgroundResource(R.drawable.message_text_background);
            holder.messageText.setTextColor(Color.WHITE);
            Glide.with(context).load(userImage).apply(RequestOptions.circleCropTransform()).into(holder.profileImage);
        }
        holder.messageText.setText(msg.getMessage());
    }

    @Override
    public int getItemCount() {
        return mMessagesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView messageText;
        private ImageView profileImage;

        private ViewHolder(View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.message_text);
            profileImage = itemView.findViewById(R.id.message_item_image);
        }
    }

}
